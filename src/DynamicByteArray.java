package ccnToolPackage;

import java.util.*;
import java.lang.*;
import ccnToolPackage.*;

public class DynamicByteArray{
	private int maxLen = 0;
	private int curLen = 0;
	private byte [] byteData = new byte [maxLen];
	private byte [] tempData = null;

	public void putData(byte [] newData){
		int len = newData.length;
		if(curLen + len > maxLen){
			maxLen = (curLen + len) * 2;
			tempData = new byte [maxLen];
			System.arraycopy(byteData, 0, tempData, 0, curLen);
			byteData = tempData;
			tempData = null;
		}
		System.arraycopy(newData, 0, byteData, curLen, len);
		curLen += len;
	}

	public byte [] getData(){
		return Arrays.copyOfRange(byteData, 0, curLen);
	}
}
