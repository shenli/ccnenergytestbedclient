package ccnToolPackage;

import ccnToolPackage.*;
import java.sql.Timestamp;
import java.util.*;
import java.lang.Double;
import java.lang.String;
import com.google.gson.*;
import com.google.gson.reflect.*;

/**
 *Energy testbed Data type: the class of return value for queries
 */
public class EtData{

	//creation Time of this piece of data
	private Timestamp birthTime; 
	private HashMap<String, HashMap<String, HashMap<String, Double>>> dataMap;
	private byte[] data;

	/**
	 * Default scheduler, create the DataMap object
         */
	public EtData() {
		Date today = new Date();
		this.birthTime = new Timestamp(today.getTime());
		this.dataMap = null;
		this.data = null;
	}

        /**
         * Recreate the object with a serialized version of the object.
         */
	public EtData(String data) {
		int index = data.indexOf("$$");
		String timeStamp = data.substring(0, index);
		String mapData = data.substring(index+2);
		this.birthTime = Timestamp.valueOf(timeStamp);
		Gson gson = new Gson();
		//Type dictType = new TypeToken<HashMap<String, HashMap<String, HashMap<String, Double>>>>() {}.getType();
		this.dataMap = gson.fromJson(mapData, 
					(new TypeToken<HashMap<String, HashMap<String, HashMap<String, Double>>>>() {}.getType()) );
		this.data = null;
	}

	/**
	 * This method serialize the data in the object.
	 */
	private void serializeSelf() {
		Gson gson = new Gson();
		String map = gson.toJson(this.dataMap);
		String time = this.birthTime.toString();
		String strData = time + "$$" + map;
		this.data = strData.getBytes();
	}

	/**
	 * This method read data from offset, put it into buffer, and returns how many bytes have been read.
	 * @param buffer	the caller pass this byte pointer to store data.
	 * @param len		buffer length.
	 * @param offset	start offset.
	 * @return		return how many bytes have been read
	 */
	public int read(byte [] buffer, int bufLen, int offset){
		if (this.data == null) 
			this.serializeSelf();
		int length = this.data.length;
		int remains = length - offset;
		int readCnt = 0;
		if (remains <= 0) {
			return 0;
		} else if (remains > bufLen) {
			readCnt = bufLen;
		} else {
			readCnt = remains;
		}
		System.arraycopy(data, offset, buffer, 0, readCnt);
		return readCnt;
	}

	/**
	 * This method reads the data tagged with nodeName, deviceName and attrName.
	 *
	 * @param nodeName	tarekc01 .. tarekc40
	 * @param deviceName	cpu, crac, sensor, node
	 * @param attrName	cpu:freq/util/temp; crac:setpoint/humid/temp; sensor:val
	 * @return the value or NaN if 
	 */
        public double readData(String nodeName, String deviceName, String attrName) {
		if (this.dataMap == null)
			return Double.NaN;
		HashMap<String, HashMap<String, Double>> nodeValues = this.dataMap.get(nodeName);
		if (nodeValues == null)
			return Double.NaN;
		HashMap<String, Double> deviceValues = nodeValues.get(deviceName);
		if (deviceValues == null)
			return Double.NaN;
		Double attrValues = deviceValues.get(attrName);
		if (attrValues == null)
			return Double.NaN;
		return attrValues;
        }

	public void putData(String nodeName, String deviceName, String attrName, Double value) {
		if (this.dataMap == null) {
			this.dataMap = new HashMap<String, HashMap<String, HashMap<String, Double>>>();
		}
		HashMap<String, HashMap<String, Double>> nodeValues = this.dataMap.get(nodeName);
		if (nodeValues == null) {
			nodeValues = new HashMap<String, HashMap<String, Double>>();
			this.dataMap.put(nodeName, nodeValues);
		}
		HashMap<String, Double> deviceValues = nodeValues.get(deviceName);
		if (deviceValues == null) {
			deviceValues = new HashMap<String, Double>();
			nodeValues.put(deviceName, deviceValues);
		}
		deviceValues.put(attrName, value);
	}

	/**
	 * This method returns the creation time of this piece of data. 
	 */
	public Timestamp getBirthTime(){
		return birthTime;
	}
}

