package ccnToolPackage;

import ccnToolPackage.*;
import java.util.*;
/**
 *ccnClientProxy translate user queries into ccn interest and fetch corresponding data
 */
public class ccnClientProxy{
	
	/**
	 * This function sends out interest to fetch data under name "/uiuc.edu/apps/energy_testbed/nodeName/devName/attrName"
	 * @param nodeName	indicates the node name, e.g., "tarekc11", "tareka02", "CRAC", and so on. An interest with node name "all" will fetch information from all nodes. 
	 * @param devName	device name, e.g., cpu, "temperature_sensor", "power_meter". An interest with device name "all" will fetch data from all available devices.
	 * @param attrName 	attibute Name, e.g., "temperature", "frequency", "power", and so on. An interest with attribute name all will fetch all available readings from corresponding node/nodes and device/devices.
	 */
	public EtData normalQuery(String nodeName, String devName, String attrName ){
		ccnGetData curGetData = new ccnGetData("normal", nodeName, devName, attrName);
		return curGetData.doAction();
	}


	/**
	 * This method triggers three rounds of information passing.
	 * 1) The client pass the aggregated description name to the server by encoding the name into interest. The server  response data indicating the target data name.
	 * 2) The server then pull the aggregated description from client by using the aggregated description name acquired from previous interest. And the server generates the target data according to the description.
	 * 3) The Client pull the target data from server by using the teh target data name acquired from the first round.
	 * Information flows are deliberately designed so that the listener will always be ready before there is corresponding call.
	 * @param ad		the aggregated description object, which includes which machines, which devices and which attributes the client is interested in.
	 */
	public EtData aggregatedQuery(AggregatedDescription ad){
		ccnGetData curGetData = new ccnGetData("aggregated", ad);
		return curGetData.doAction();
	}


	/**
	 *This function sends out a control interest to energy testbed server.
	 * @param attrValue	describe the new set point of a corresponding knob.
	 */
	public EtData normalControl(String nodeName, String devName, String attrName, String attrValue){
		ccnGetData curGetData = new ccnGetData("control", nodeName, devName, attrName, attrValue);
                return curGetData.doAction();
	}

	//<For testing begin>
	public static void main(String[] args){
		ccnClientProxy curClient = new ccnClientProxy();
		EtData curData = curClient.normalQuery("tarekc12", "cpu", "utilization");
		//System.out.println(curData.data2Str());
		curData = curClient.normalControl("tarekc13", "cpu", "frequency", "1995000");
		//System.out.println(curData.data2Str());
		curData = curClient.aggregatedQuery(new AggregatedDescription());
		//System.out.println(curData.data2Str());

		System.exit(0);
	}
	//<For testing end>

}
