package ccnToolPackage;

import ccnToolPackage.*;
import java.util.*;
import java.sql.Timestamp;

import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;

import org.ccnx.ccn.CCNFilterListener;
import org.ccnx.ccn.CCNHandle;
import org.ccnx.ccn.config.ConfigurationException;
import org.ccnx.ccn.impl.support.Log;
import org.ccnx.ccn.io.CCNFileInputStream;
import org.ccnx.ccn.io.CCNInputStream;
import org.ccnx.ccn.io.CCNOutputStream;
import org.ccnx.ccn.protocol.CCNTime;
import org.ccnx.ccn.protocol.ContentName;
import org.ccnx.ccn.protocol.Exclude;
import org.ccnx.ccn.protocol.ExcludeComponent;
import org.ccnx.ccn.protocol.Interest;
import org.ccnx.ccn.protocol.MalformedContentNameStringException;


import org.ccnx.ccn.utils.CommonSecurity;


/**
 * A command-line utility for pulling files out of ccnd or a repository.
 * Note class name needs to match command name to work with ccn_run
 */
public class ccnGetData {
	
	public static final int NORMAL_QUERY = 0;
	public static final int AGGREGATED_QUERY = 1;
	public static final int NORMAL_CONTROL = 2;
	public static final int BUF_SIZE = 8192;
	public static final int DATA_NAME_LEN = 4096;
	public static final String configFile = "../conf/userPrefix.conf";

	private String logName = null;
	private static String dirName = "../log/";
	private static String ccnxURI = "ccnx:/uiuc.edu/apps/energy_testbed/";
	private int action = -1;	

	private AggregatedDescription _ad = null;
	private ContentName _requestName = null;
	private ContentName _descriptionName = null;
	private ContentName _prefix = null;
	private DescriptionListener _descriptionListener = null;

	public static Integer timeout = null;
	public static boolean unversioned = false;

	/**
	 * Constructor for normal request.
	 */
	public ccnGetData(String actionName, String machineName, String deviceName, String attributeName){
		this(actionName, machineName + "/" + deviceName + "/" + attributeName);	
		action = NORMAL_QUERY;
	}

	/**
	 * Constructor for aggregated request.
	 */
	public ccnGetData(String actionName,  AggregatedDescription ad){
		this(actionName, "");
		action = AGGREGATED_QUERY;
		long curTime = System.currentTimeMillis();
		String strPrefix = ccnGetPrefix();
		String strDescriptionName = strPrefix + "/nodeList/" + curTime;
		_ad = ad;
		try{
			_prefix = ContentName.fromURI(strPrefix);
			_descriptionName = ContentName.fromURI(_prefix.toURIString() + "/nodeList/"  + curTime);
			_requestName = ContentName.fromURI(
					_requestName.toURIString() + "/" + 
					strDescriptionName
					);
			myLog("In aggregated constructor : start ---------------\n");
			myLog("_prefix is : " + _prefix.toURIString() + "\n");
			myLog("_descriptionName is : " + _descriptionName.toURIString() + "\n");
			myLog("_requestName is : " + _requestName.toURIString() + "\n");
			myLog("In aggregated constructor : end -------------------\n");
		}
		catch(Exception ex){
			System.out.println("Exception In ccnGetData constructor : constructing object for aggregated query!");
			ex.printStackTrace();
		}	

	}

	/**
	 * Constructor for control request.
	 */
	public ccnGetData(String actionName, String machineName, String deviceName, String attributeName, String newValue){
		this(actionName, machineName + "/" + deviceName + "/" + attributeName + "/" + newValue);		
		action = NORMAL_CONTROL;
	}


	public ccnGetData(String requestType, String requestDetail){
		long curTime = System.currentTimeMillis();
		String request = requestType + "/" + curTime + "/" + requestDetail;

		try{
			_requestName = ContentName.fromURI(ccnxURI + request);
		} catch (MalformedContentNameStringException e) {
			System.out.println("Malformed name: " + request + " " + e.getMessage());
			e.printStackTrace();
		}


		logName = "ccnGetData_log_" + curTime + ".txt";
		//make output directory
		File outFile = new File(dirName);
		try{
			if(!(outFile.exists())){
				outFile.mkdirs();
			}
			myLog("file Created!\n");
		}
		catch(Exception ex){
			System.out.println("In Constructor: fail to create log director!\n");
		}

		
	}

	public String ccnGetPrefix(){
		String ccnPrefix = null;
		try{
			FileReader fr = new FileReader(configFile);
			BufferedReader br = new BufferedReader(fr);
			ccnPrefix = br.readLine();
			br.close();
		}
		catch(IOException ex){
			System.out.println("In ccnGetPrefix : IOException : " + ex.getMessage());
			ex.printStackTrace();
		}
		catch(Exception ex){
			System.out.println("In ccnGetPrefix : Exception : " + ex.getMessage());
			ex.printStackTrace();
		}
		return ccnPrefix;	
	}

	public  void  myLog(String data){
                writeToFile(dirName + logName, data);
        }

        public  void writeToFile(String filename, String data){
                try{
                        File file = new File(filename);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.write(data);
                        fileWriter.close();
                }
                catch(Exception ex){
                        Log.info("In writeToFile Exception ");
                }
        }

	public void openDescriptionListener(ContentName descriptionName){
		myLog("In openNodeListListener : nameListname is " + descriptionName.toURIString() + "\n");
		_descriptionListener = new DescriptionListener(_descriptionName);
		_descriptionListener.start();
	}

	public void closeDescriptionListener(){
	}

	public ContentName getAggregatedDataName(ContentName requestName){
		try{
			CCNHandle handle = CCNHandle.open();
			CCNInputStream input = new CCNInputStream(requestName, handle);
			
			byte [] buffer = new byte[DATA_NAME_LEN];
			byte [] tmpBuffer = new byte[DATA_NAME_LEN];
			int readCnt = 0;
			int totalCnt = 0;

			myLog("In getAggregatedDataName : Start Reading\n");

			readCnt = input.read(tmpBuffer);
			while(readCnt > 0){
				System.arraycopy(tmpBuffer, 0, buffer, totalCnt, readCnt);
				totalCnt = totalCnt + readCnt;
				readCnt = input.read(tmpBuffer);
			}
			input.close();	
			handle.close();			
			return  ContentName.fromURI(new String(Arrays.copyOfRange(buffer, 0, totalCnt)));
		}
		catch (ConfigurationException e) {
			System.out.println("Configuration exception in ccnGetData: " + e.getMessage());
			e.printStackTrace();
		}
		catch (MalformedContentNameStringException e){
			System.out.println("Exception in getAggregatedDataName : MaFormedContentName\n");
			e.printStackTrace();

		}
		catch (IOException e) {
			System.out.println("Cannot write file or read content. " + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public EtData getData(ContentName requestName){
		try{
			CCNHandle handle = CCNHandle.open();
			CCNInputStream input = new CCNInputStream(requestName, handle);

			byte [] buffer = new byte[BUF_SIZE];
			DynamicByteArray dba = new DynamicByteArray();

			int readCnt = 0;
			int readTotal = 0;
			myLog("Start Reading\n");
			readCnt = input.read(buffer);
			while(readCnt > 0){
				myLog("In reading : readCnt is " + readCnt + "\n");
				dba.putData(Arrays.copyOfRange(buffer, 0, readCnt));
				readTotal = readTotal + readCnt;
				readCnt = input.read(buffer);
			}
			input.close();
			handle.close();
			myLog("Read done!\n");
			System.out.println("Read Done!\n");
			//System.out.println("Data is : " + curData.data2Str() + "\n");
			//myLog("result data is : " + curData.data2Str() + "\n");
			return new EtData(new String(dba.getData()));
		}
		catch (ConfigurationException e) {
			System.out.println("Configuration exception in ccnGetData: " + e.getMessage());
			e.printStackTrace();
		}
		catch (IOException e) {
			System.out.println("Cannot write file or read content. " + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public EtData doAction(){
		if(AGGREGATED_QUERY == action){
			myLog("In doAction : AGGREGATED_QUERY\n");
			openDescriptionListener(_descriptionName);
			ContentName aggregatedDataName = getAggregatedDataName(_requestName);
			myLog("Data name is :  " + aggregatedDataName.toURIString() + "\n");
			return getData(aggregatedDataName);
		}
		else if (NORMAL_QUERY == action || NORMAL_CONTROL == action){
			return getData(_requestName);
		}
		return null;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AggregatedDescription ad = new AggregatedDescription();
		ad.setData("I am the description setted in ccnGetData main()");
		ccnGetData curGetData = new ccnGetData("aggregated", ad);
		curGetData.doAction();
		System.exit(0);
	}

	class DescriptionListener implements CCNFilterListener{
		private ContentName _descriptionName = null;
		private CCNHandle _handle = null;
		public DescriptionListener(ContentName descriptionName){
			_descriptionName = descriptionName;
			try{
				_handle = CCNHandle.open();
			}
			catch(Exception ex){
				myLog("In DescriptionListener constructor : CCNHandle open exception" + ex.getMessage());
				ex.printStackTrace();
			}
			myLog("In DescriptionListener constructor : constructed\n");
		}
		public void start(){
			myLog("In Descriptionener start: register prefix : " + _descriptionName.toURIString() + "\n");
			try{
				_handle.registerFilter(_descriptionName, this);
			}
			catch(IOException ex){
				System.out.println("In DescriptionListener start : IO Exception " + ex.getMessage());
				ex.printStackTrace();
			}
		}
		public boolean handleInterest(Interest interest){
			myLog("In DescriptionListener handleInterest : received interest : " + interest.name().toURIString() + "\n");
			try{		
				CCNOutputStream ccnout = new CCNOutputStream(interest.name(), _handle);
				ccnout.addOutstandingInterest(interest);

				byte [] buffer = new byte [BUF_SIZE];
				int offset = 0;
				int readCnt = _ad.read(buffer, BUF_SIZE, offset); 

				while(readCnt > 0){
					ccnout.write(buffer, 0, readCnt);
					myLog("In DescriptionListener handleInterest : readCnt is : " + readCnt + "\n");
					offset = offset + readCnt;
					readCnt = _ad.read(buffer, BUF_SIZE, offset);
				} 
				myLog("In DescriptionListener : Transmission done\n");
				ccnout.close();
			}
			catch(IOException ex){
				myLog("In DescriptionListener handleInterest : IOException " + ex.getMessage() + "\n");
				ex.printStackTrace();
			}	
			return true;
		}
		public void shutdown(){
			if(null != _handle){
				_handle.unregisterFilter(_descriptionName, this);
				myLog("In NodeListListerner shutdown : handle unregistered\n");
			}
			else{
				myLog("In NodeListListerner shutdown : handle already unregistered\n");

			}
		}	
	}

}
