package ccnToolPackage;

import java.util.Timer;
import java.util.TimerTask;
import java.lang.*;

public class TestServerProxy{

	public static ccnServerProxy ccnServer = null;

	public static void main(String args []){
		ccnServer = new ccnServerProxy();
		EtData curData  = ccnServer.solveNormalQuery("all","all","all");
		System.out.println("Got Data!");
		String name = null;
		for(int i = 1 ; i <= 40; ++i){
			name = getNodeName(i);
			System.out.print("Node_" + i + " : ");
			System.out.print("freq = " + curData.readData(name, "cpu", "frequency") + ", ");
			System.out.print("util = " + curData.readData(name, "cpu", "utilization") + ", ");
			System.out.print("temp = " + curData.readData(name, "cpu", "temperature") + ", ");
			System.out.print("power= " + curData.readData(name, "power", "state") +  ".\n ");		
		}

		System.out.println("energy : " + curData.readData("rack", "outlet", "power"));
		System.out.println("CRAC in : " + curData.readData("CRAC", "sensor", "intake"));
		System.out.println("CRAC out : " + curData.readData("CRAC", "sensor", "outlet"));

		ccnServer.solveNormalControl("tarekc09", "cpu", "frequency", "2394");
		ccnServer.solveNormalControl("tarekc10", "program", "test", "stop");
/*
		for(int i = 1; i <= 40; ++i){
			name = getNodeName(i);
			System.out.println("In node : " + name);			
			curData = ccnServer.solveNormalQuery(name, "cpu", "utilization");
			System.out.print("util = " + curData.readData(name, "cpu", "utilization") + ", ");
			curData = ccnServer.solveNormalQuery(name, "cpu", "frequency");
			System.out.print("freq = " + curData.readData(name, "cpu", "frequency") + ", ");
			curData = ccnServer.solveNormalQuery(name, "cpu", "temperature");
			System.out.print("temp = " + curData.readData(name, "cpu", "temperature") + ".\n");
			curData = ccnServer.solveNormalQuery(name, "power", "state");
			System.out.print("power = " + curData.readData(name, "power", "state") + ".\n");	
		}

		curData = ccnServer.solveNormalQuery("CRAC", "sensor", "intake");
		System.out.println("CRAC intake : " + curData.readData("CRAC", "sensor", "intake"));
		curData = ccnServer.solveNormalQuery("CRAC", "sensor", "outlet");
		System.out.println("CRAC outlet : " + curData.readData("CRAC", "sensor", "outlet"));
*/
	}

	public static String getNodeName(int id){
		if(id < 10)
			return "tarekc0" + id;
		else
			return "tarekc" + id;
	}

}
