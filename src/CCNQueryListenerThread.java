package ccnToolPackage;

import java.util.*;
import java.lang.*;
import ccnToolPackage.*;
import org.ccnx.ccn.io.CCNVersionedOutputStream;

import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.ccnx.ccn.CCNFilterListener;
import org.ccnx.ccn.CCNHandle;
import org.ccnx.ccn.config.ConfigurationException;
import org.ccnx.ccn.impl.support.Log;
import org.ccnx.ccn.io.*;
import org.ccnx.ccn.profiles.CommandMarker;
import org.ccnx.ccn.profiles.SegmentationProfile;
import org.ccnx.ccn.profiles.VersioningProfile;
import org.ccnx.ccn.profiles.metadata.MetadataProfile;
import org.ccnx.ccn.profiles.nameenum.NameEnumerationResponse;
import org.ccnx.ccn.profiles.nameenum.NameEnumerationResponse.NameEnumerationResponseMessage;
import org.ccnx.ccn.profiles.nameenum.NameEnumerationResponse.NameEnumerationResponseMessage.NameEnumerationResponseMessageObject;
import org.ccnx.ccn.profiles.security.KeyProfile;
import org.ccnx.ccn.protocol.CCNTime;
import org.ccnx.ccn.protocol.ContentName;
import org.ccnx.ccn.protocol.Exclude;
import org.ccnx.ccn.protocol.ExcludeComponent;
import org.ccnx.ccn.protocol.Interest;
import org.ccnx.ccn.protocol.MalformedContentNameStringException;

public class CCNQueryListenerThread implements Runnable {

	public static final int TIME_NAME 	= 	1;
	public static final int MACHINE_NAME 	= 	2;
	public static final int DEVICE_NAME 	= 	3;
	public static final int ATTRIBUTE_NAME 	=	4;
	public static final int NEW_VALUE 	= 	5;
	public static final int NODE_LIST_START = 	3;

	private String logName = null;
	private String dirName = "log/";
	private String ccnxURI = "ccnx:/uiuc.edu/apps/energy_testbed/";
	private String queryURI = "ccnx:/uiuc.edu/apps/energy_testbed/normal";
	private String aggregatedServerName = "ccnx:/uiuc.edu/apps/aggregated_server/";

	static String DEFAULT_URI = "ccnx:/";
	static int BUF_SIZE = 4096;


	private QueryListener queryListener = null;	

	public  void  myLog(String data){
		writeToFile(dirName + logName, data);
	}

	public  void writeToFile(String filename, String data){
		try{
			File file = new File(filename);
			FileWriter fileWriter = new FileWriter(file, true);
			fileWriter.write(data);
			fileWriter.close();
		}
		catch(Exception ex){
			Log.info("In writeToFile Exception ");
		}
	}



	private void transmitData(EtData data, Interest interest, CCNHandle handle){
		//Add the creation time of this piece of data into the version name

		//CCNTime birthTime = new CCNTime(data.getBirthTime());

		try{
			CCNOutputStream ccnout = new CCNOutputStream(interest.name(), handle);

			//register the outstanding interest
			ccnout.addOutstandingInterest(interest);

			byte [] buffer = new byte[BUF_SIZE];
			int offset = 0;
			int readCnt = data.read(buffer, BUF_SIZE, offset);
			myLog("Start sending data\n");
			while (readCnt >0){
				ccnout.write(buffer, 0, readCnt);
				myLog("Sending data, readCnt = " + readCnt + "\n");
				offset = offset + readCnt;
				readCnt = data.read(buffer, BUF_SIZE, offset);
			}
			myLog("Data Transmission done!\n");
			ccnout.close();
		}
		catch(IOException ex){
			myLog("In transmitData: IOException: " + ex.getMessage());
			ex.printStackTrace();
		}

	}


	public void run(){
		try{
			queryListener = new QueryListener();
			queryListener.start();
		}
		catch(Exception ex){
			System.out.println("EXCEPTION: in query listener run : " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	public static void main(String args []){
		Thread thread = new Thread(new CCNQueryListenerThread());
		thread.start();
	}

	class QueryListener implements CCNFilterListener{
		String _machineName, _deviceName, _attributeName, _newValue;
		Interest _interest;
		CCNHandle _handle;
		ContentName _prefix;


		public QueryListener() throws MalformedContentNameStringException, ConfigurationException, IOException {

			_prefix = ContentName.fromURI(queryURI);
			_handle = CCNHandle.open();


			logName = "QueryListener_log_" + System.currentTimeMillis() + ".txt";
			//make output directory
			File outFile = new File(dirName);
			try{
				if(!(outFile.exists())){
					outFile.mkdirs();
				}
				myLog("file Created!\n");
			}
			catch(Exception ex){
				System.out.println("In Constructor: fail to create output director!\n");
			}
		}

		public void start() throws IOException{
			myLog("QueryListern has started!\n");
			// All we have to do is say that we're listening on our main prefix.
			_handle.registerFilter(_prefix, this);
		}



		public boolean handleInterest(Interest interest) {
			// Alright, we've gotten an interest. Either it's an interest for a stream we're
			// already reading, or it's a request for a new stream.
			myLog("received Interest : " + interest.name().toURIString() + "\n");

			if (SegmentationProfile.isSegment(interest.name()) && !SegmentationProfile.isFirstSegment(interest.name())) {
				System.out.println("Got an interest for something other than a first segment, ignoring : " + interest.name().toURIString());
				return false;
			} 
			else if (MetadataProfile.isHeader(interest.name())) {
				System.out.println("Got an interest for the first segment of the header, ignoring : " + interest.name().toURIString());
				return false;
			}

			return answerRequest(interest);
		}



		/**
		 * Turn off everything.
		 * @throws IOException 
		 */
		public void shutdown() throws IOException {
			if (null != _handle) {
				_handle.unregisterFilter(_prefix, this);
				myLog("QueryListener Closed!\n");
			}
		}



		public boolean answerRequest(Interest interest){
			myLog("In queryHandler\n");

			ContentName requestName = interest.name().postfix(_prefix);
			String request = requestName.toURIString();


			myLog("Start Handle Request : request is " + request + "\n");

			int i = 0;
			//extract information from request name
			String [] splitRes = request.split("/");
			for(i = 0 ; i < splitRes.length; ++i){
				myLog(i + " " + splitRes[i] + "\n");
			}


			String machineName = splitRes[MACHINE_NAME];
			String deviceName = splitRes[DEVICE_NAME];
			String attributeName = splitRes[ATTRIBUTE_NAME];
			myLog("normal query: " + machineName + " " + deviceName + " " + attributeName + "\n");

			ccnServerProxy server = new ccnServerProxy();
			EtData data = server.solveNormalQuery(machineName, deviceName, attributeName);
			transmitData(data, interest, _handle);
			return true;
		}

	}

}


