package ccnToolPackage;

import ccnToolPackage.EtData;
import java.util.*;
import java.net.*;
import org.apache.xmlrpc.client.*;
import com.google.gson.*;
import com.google.gson.reflect.*;

/**
 *ccnClientProxy translate user queries into ccn interest and fetch corresponding data
 */
public class ccnServerProxy{

	XmlRpcClient server_proxy = null;
	String[] machines = null;

	public ccnServerProxy() {
		try {
			server_proxy = getProxy();
		} catch (Exception e) {
			System.out.println("Exception caught: " + e.getMessage());
			server_proxy = null;
		}
	}

	private XmlRpcClient getProxy() throws Exception {
                XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
                config.setServerURL(new URL("http://tarekc20.cs.uiuc.edu:13443"));
                XmlRpcClient client = new XmlRpcClient();
                client.setConfig(config);
		return client;
	}

	/**
	 * This function generate data for interest with name "/uiuc.edu/apps/energy_testbed/nodeName/devName/attrName"
	 * @param nodeName	indicates the node name, e.g., "tarekc11", "tareka02", "CRAC", and so on. An interest with node name "all" will fetch information from all nodes. 
	 * @param devName	device name, e.g., cpu, "temperature_sensor", "power_meter". An interest with device name "all" will fetch data from all available devices.
	 * @param attrName 	attibute Name, e.g., "temperature", "frequency", "power", and so on. An interest with attribute name all will fetch all available readings from corresponding node/nodes and device/devices.
	 */
	public EtData solveNormalQuery(String nodeName, String devName, String attrName){
		EtData result = new EtData();
		try {
			nodeQueryHandler(result, nodeName, devName, attrName);
		} catch (Exception e) {
			System.out.println("ERROR ERROR ERROR!!!" + e.getMessage());
		}
		return result;
	}

	private void nodeQueryHandler(EtData result, String nodeName, String devName, String attrName) throws Exception {
		if (devName.equals("all")) {
			cpuQueryHandler(result, nodeName, "cpu", attrName);
			cracQueryHandler(result, "CRAC", "sensor", attrName);
			machineQueryHandler(result, nodeName, "power", attrName);
			powerQueryHandler(result, "rack", "outlet", "power");
		} else if (devName.equals("cpu")) {
			cpuQueryHandler(result, nodeName, "cpu", attrName);
		} else if (devName.equals("sensor")) {
			cracQueryHandler(result, "CRAC", "sensor", attrName);
		} else if (devName.equals("power")) {
			machineQueryHandler(result, nodeName, "power", attrName);
		} else if (devName.equals("outlet")) {
			powerQueryHandler(result, "rack", "outlet", "power");
		}
	}

	private void powerQueryHandler(EtData result, String nodeName, String devName, String attrName) throws Exception {
		Double r = (Double) server_proxy.execute("power_read_sum", new Object[] {});
		result.putData(nodeName, devName, attrName, r);
	}

	private void cpuQueryHandler(EtData result, String nodeName, String devName, String attrName) throws Exception {
		Object[] params = new Object[] { new String(nodeName) };
		String r = (String) server_proxy.execute("cpu_read_summary", params);
		Gson gson = new Gson();
		HashMap<String, String>[] cpu_datas = gson.fromJson(r, (new TypeToken<HashMap<String, String>[]>() {}.getType()) );
                for (HashMap<String, String> cpu_data: cpu_datas) {
			if (cpu_data.get("node") == null)
				continue;
			result.putData(cpu_data.get("node"), devName, "frequency", Double.parseDouble(cpu_data.get("freq")));
			result.putData(cpu_data.get("node"), devName, "temperature", Double.parseDouble(cpu_data.get("temp")));
			result.putData(cpu_data.get("node"), devName, "utilization", Double.parseDouble(cpu_data.get("util")));
		}
	}

	private void cracQueryHandler(EtData result, String nodeName, String devName, String attrName) throws Exception {
		//String r = (String) server_proxy.execute("crac_read_status", new Object[] {});
		Gson gson = new Gson();
		/*
		HashMap<String, String> crac_data = gson.fromJson(r, (new TypeToken<HashMap<String, String>>() {}.getType()) );
                result.putData(nodeName, devName, "setpoint", Double.parseDouble(crac_data.get("setpoint")));
                result.putData(nodeName, devName, "temperature", Double.parseDouble(crac_data.get("temperature")));
                result.putData(nodeName, devName, "humidity", Double.parseDouble(crac_data.get("humidity")));
		*/
		String r = (String) server_proxy.execute("crac_read_sensor", new Object[] {});
		HashMap<String, String> crac_data = gson.fromJson(r, (new TypeToken<HashMap<String, String>>() {}.getType()) );
		result.putData(nodeName, devName, "intake", Double.parseDouble(crac_data.get("intake")));
		result.putData(nodeName, devName, "outlet", Double.parseDouble(crac_data.get("outake")));
	}

	private void machineQueryHandler(EtData result, String nodeName, String devName, String attrName) throws Exception {
		String r = (String) server_proxy.execute("machine_check_status", new Object[] { new String(nodeName) });
		Gson gson = new Gson();
		HashMap<String, Boolean> machine_status = gson.fromJson(r, (new TypeToken<HashMap<String, Boolean>>() {}.getType()) );
		for (String key: machine_status.keySet()) {
			if (machine_status.get(key)) {
				result.putData(key, devName, "state", 1.0);
			} else {
				result.putData(key, devName, "state", 0.0);
			}
		}
	}

	/**
	 * This function generates the data according to the AggregatedDescrption objects
	 * @param ad		a data object that describes users query, including node list, device list, attribute list.
	 */
	public EtData solveAggregatedQuery(AggregatedDescription ad){
		return null;
	}

	/*
	 *This function sends out a control interest to energy testbed server.
	 * @param attrValue	describe the new set point of a corresponding knob.
	 */
	public EtData solveNormalControl(String nodeName, String devName, String attrName, String attrValue) {
		try {
			nodeControlHandler(nodeName, devName, attrName, attrValue);
		} catch (Exception e) {
			System.out.println("ERROR! ERROR!! ERROR!!! ERROR!!!!" + e.getMessage());
		}
		return null;
	}

	private void nodeControlHandler(String nodeName, String devName, String attrName, String attrValue) throws Exception {
		if (devName.equals("cpu")) 
			cpuControlHandler(nodeName, attrName, attrValue);
		else if (devName.equals("power"))
			machineControlHandler(nodeName, attrName, attrValue);
		else if (devName.equals("program"))
			execControlHandler(nodeName, attrName, attrValue);
	}

        private void cpuControlHandler(String nodeName, String attrName, String attrValue) throws Exception {
		if (attrName.equals("frequency")) {
			Integer value = new Integer(attrValue);
			Object[] params = new Object[] { new String(nodeName), new Integer(value) };
			server_proxy.execute("cpu_change_freq", params);
		}
	}

	public void  machineControlHandler(String nodeName, String attrName, String attrValue) throws Exception {
		if (attrName.equals("state")) {
			if (attrValue.equals("true")) {
				Object[] params = new Object[] { new String(nodeName) };
				server_proxy.execute("machine_turn_on", params);
			} else if (attrValue.equals("false")) {
				Object[] params = new Object[] { new String(nodeName) };
				server_proxy.execute("machine_turn_off", params);
			}
		}
	}

	public void execControlHandler(String nodeName, String attrName, String attrValue) throws Exception {
		if (attrName.equals("test")) {
			if (attrValue.equals("start")) {
				Object[] params = new Object[] { new String(nodeName) };
				server_proxy.execute("payload_start", params);
			} else if (attrValue.equals("stop")) {
				Object[] params = new Object[] { new String(nodeName) };
				server_proxy.execute("payload_stop", params);
			}
		}
	}
}
