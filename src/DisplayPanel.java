package ccnToolPackage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;
import java.lang.Math;

public class DisplayPanel extends JPanel{
	public static final int LENTH = 500;
	public static final int WIDTH = 120;
	private static final int GRID_VER_STEP = 30;
	private static final int GRID_HOR_STEP = 50;
	private static final int CURVE_STEP = 5;
	private static final int MAX_DATA_NUM = 2;

	private int maxValue = 0;
	private int minValue = 0;
	private float [][] data = null;
	private int curPos = 0;
	private int dataNum = 0;
	private float [] nextData = null;
	private Timer timer = null;
	private static final Color [] dataColor = {Color.GREEN, Color.YELLOW};

	public DisplayPanel(int dataNum, int minValue, int maxValue){
		this.dataNum = dataNum;		
		this.maxValue = maxValue;
		this.minValue = minValue;

		data = new float[dataNum][LENTH/CURVE_STEP];
		nextData = new float[dataNum];
		initData();
		initGUI();
	}

	public void startDisplay(){
		if(null == timer)
			timer = new Timer(true);
		timer.schedule(new InsertData(), 0, 2010);
	}

	public void stopDisplay(){
		if(null != timer){
			timer.cancel();
		}
		initData();		
		repaint();
	}

	private void initData(){
		for(int j = 0; j < dataNum; ++j){
			nextData[j] = -1;
			for(int i = 0; i < data[0].length; ++i)
				data[j][i] = -1;
		}
		curPos = 0;
	}

	private void initGUI(){
		this.setPreferredSize(new Dimension(LENTH, WIDTH));
		this.setBackground(Color.BLACK);
	}

	//override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		drawGrid(g);
		drawData(g);
	}

	private void drawGrid(Graphics g){
		//draw horizontal line
		int pos = 0;
		g.setColor(Color.WHITE);
		while(true){
			pos = pos + GRID_VER_STEP;
			if(pos >= WIDTH)
				break;
			g.fillRect(0, pos, LENTH, 1);
		}
		//draw vertical line
		pos = 0;	
		while(true){
			pos = pos + GRID_HOR_STEP;
			if(pos >= LENTH)
				break;
			g.fillRect(pos, 0, 1, WIDTH);
		}
	}

	private int getX(int cnt){
		return LENTH - cnt * CURVE_STEP;
	}

	private int getY(float data){
		return WIDTH - (int)((data - minValue) * WIDTH / (maxValue - minValue));
	}

	private void drawData(Graphics g){
		int length = data[0].length;
		int row = 0, cnt = 0, pos = 0, prePos = 0, x = 0, y = 0, preX = 0, preY = 0;
		for( row = 0; row < dataNum; ++row){ 		
			g.setColor(dataColor[row]);
			for( cnt = 1; cnt < length; ++ cnt){
				pos = (curPos + cnt) % length;
				prePos = (pos-1 + length) % length;
				if(data[row][pos] < 0 || data[row][prePos] < 0)
					continue;
				x = getX(cnt);
				preX = getX(cnt - 1);
				y = getY(data[row][pos]);
				preY = getY(data[row][prePos]);
				g.drawLine(x, y, preX, preY);
			}
		}
	}

	public void setNextData(float data, int row){
		this.nextData[row] = data;
	}

	class InsertData extends TimerTask{
		public void run(){
			for(int i = 0 ; i < dataNum; ++i){
//				setNextData((int)(Math.random() * maxValue), i);
				data[i][curPos] = nextData[i];
				if(nextData[i] >= 0)
					nextData[i] = -1;

				//make up the lost data
				if(data[i][curPos] < 0){
					int prePos = (curPos + 1) % data[0].length;
					data[i][curPos] = data[i][prePos];
				}
			}
			curPos = (curPos - 1 + data[0].length) % data[0].length;
			repaint();
		}
	}


	public static void main(String [] args){
		JFrame jf = new JFrame();
		jf.setSize(600, 600);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel jp = new JPanel();
		jp.add(new DisplayPanel(2, 0, 6000));
		jf.add(jp);
		jf.setVisible(true);
	}
}
