package ccnToolPackage;

import javax.crypto.Cipher;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.security.PublicKey;
import java.security.PrivateKey;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;

public class ccnKeyManager{

	private static final String ALGORITHM = "RSA";
	private static final int KEY_SIZE = 512;
	private static final String PUB_KEY_FILE = "../conf/pub.key";
	private static final String PRI_KEY_FILE = "../conf/pri.key";

	private static KeyPair generateKeyPair(){
		try{
			SecureRandom sr = new SecureRandom();

			KeyPairGenerator kpg = KeyPairGenerator.getInstance(ALGORITHM);
			kpg.initialize(KEY_SIZE, sr);
			KeyPair kp = kpg.generateKeyPair();
			PublicKey publicKey = (PublicKey)kp.getPublic();
			PrivateKey privateKey = (PrivateKey)kp.getPrivate();
			ObjectOutputStream oosPublic = new ObjectOutputStream(new FileOutputStream(PUB_KEY_FILE));
			ObjectOutputStream oosPrivate= new ObjectOutputStream(new FileOutputStream(PRI_KEY_FILE));
			oosPublic.writeObject(publicKey);
			oosPrivate.writeObject(privateKey);
			oosPublic.close();
			oosPrivate.close();	
			return kp;	
		}
		catch(Exception ex){
			System.out.println("EXCEPTION: In generateKeyPair :" + ex.getMessage());
			ex.printStackTrace();		
		}
		return null;
	}

	private Key readKey(String filename){
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(PUB_KEY_FILE));
			Key key = (Key)ois.readObject();
			ois.close();
			return key;
		}
		catch(Exception ex){
			System.out.println("EXCEPTION: In readKey : " + ex.getMessage());
			ex.printStackTrace();

		}	
		return null;

	}

	public static byte [] encrypt(byte [] source, Key key){
		try{		
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return cipher.doFinal(source); 
		}
		catch(Exception ex){
			System.out.println("EXCEPTION: In encrypt : " + ex.getMessage());
			ex.printStackTrace();
		}
		return null;
	}

	public static byte[] decrypt(byte [] source, Key key){
		try{
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, key);
			return cipher.doFinal(source);
		}
		catch(Exception ex){
			System.out.println("EXCEPTION: In decrypt : " + ex.getMessage());
			ex.printStackTrace();
		}
		return null;
	}

	public static PublicKey getPublicKey(){
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(PUB_KEY_FILE));
			PublicKey publicKey = (PublicKey)ois.readObject();
			ois.close();
			if (null == publicKey){
				System.out.println("Public key is null");

			}
			return publicKey;
		}
		catch(Exception ex){
			System.out.println("EXCEPTION: In getPublicKey : " + ex.getMessage());
			ex.printStackTrace();
		}	
		return null;
	}

	public static void main(String [] args){
/*		generateKeyPair();
		String strSource = "test encrypt and decrypt";
		byte [] source = strSource.getBytes();
		byte [] target = encrypt(source);
		String strTarget = new String(target);
		System.out.println(strSource);
		System.out.println(strTarget);
		byte [] decoded = decrypt(target);
		String strDecoded = new String(decoded);
		System.out.println(strDecoded);

		PublicKey publicKey = getPublicKey();
		System.out.println("PublicKey is " + publicKey.getEncoded());
*/	}

}
