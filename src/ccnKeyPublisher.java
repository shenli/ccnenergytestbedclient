package ccnToolPackage;

import java.io.IOException; 
import java.security.PublicKey;
import java.security.PrivateKey;

import org.ccnx.ccn.impl.security.keys.BasicKeyManager;
import org.ccnx.ccn.config.ConfigurationException;

public class ccnKeyPublisher{

	private BasicKeyManager _keyManager;

	public ccnKeyPublisher(String userName, String password){
		try{
			_keyManager = new BasicKeyManager(userName, null, null, null, null, null, password.toCharArray());
			_keyManager.initialize();
			PublicKey publicKey = _keyManager.getDefaultPublicKey();
			System.out.println("Public Key is : " + publicKey.getEncoded());
			PrivateKey privateKey = _keyManager.getDefaultSigningKey();
			System.out.println("Private Key is : " + privateKey.getEncoded());
		}
		catch(ConfigurationException ex){
			System.out.println("In ccnKeyPublisher constructor : ConfigurationException : " + ex.getMessage());
			ex.printStackTrace();

		}
		catch(IOException ex){
			System.out.println("In ccnKeyPublisher constructor : IOException : " + ex.getMessage());
			ex.printStackTrace();
		}

	}
	public static void main(String args []){
		ccnKeyPublisher myKeyPub = new ccnKeyPublisher(args[0], args[1]);
	}
}

