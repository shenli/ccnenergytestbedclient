package ccnToolPackage;

import java.util.Timer;
import java.util.TimerTask;
import java.lang.*;

public class TextClient{

	public static final int ROW = 5;
	public static final int COLUMN = 8;
	public static final int NODENUM = 40;

	private boolean [] started = new boolean[NODENUM];
	private boolean [] powerOn = new boolean[NODENUM]; 
	private ccnClientProxy ccnClient = null;
	private static final int [] freqs = {2395000, 2394000, 2261000, 2128000, 1995000, 1862000, 1729000, 1596000, 1463000, 1330000, 1197000};

	public TextClient(){
		ccnClient = new ccnClientProxy();
		int cnt = 0;
		for (int i = 0; i < NODENUM;  ++i){
			started[i] = false;
			powerOn[i] = true;
		}
		while(true){
			cnt += 1;
			query();
			try{
				Thread.sleep(1000);
			}
			catch(Exception ex){
				System.out.println("EXCEPTION : In constructor : " + ex.getMessage());
				ex.printStackTrace();
			}

			if(cnt % 5 == 0){
//				control();	
			}
		}
	}

	public static void main(String [] args){
		TextClient textClient = new TextClient();
	}

	public String getNodeID(int row, int column){
		int curID = row * COLUMN + column;
		if(curID < 10)
			return "tarekc0" + curID;
		else
			return "tarekc" + curID;
	}

	public String randNodeID(){
		int row = (int)(Math.random() * ROW);
		int column = (int)(Math.random() * COLUMN);
		return getNodeID(row, column);	

	}

	public int randFreq(){
		int index = (int)(Math.random() * freqs.length);
		return freqs[index];
	}

	public String int2StrID(int id){
		if(id < 10)
			return "tarekc0" + id;
		else
			return "tarekc" + id;
	}

	public void control(){
		String nodeID = randNodeID();
		String nodeFreq =  "" + randFreq();
		ccnClient.normalControl(nodeID, "cpu", "frequency", nodeFreq);
		System.out.println("control : " + nodeID + "/cpu/frequency/" + nodeFreq);
		int proID = (int)(Math.random() * NODENUM);
		if(started[proID]){
			ccnClient.normalControl(int2StrID(proID), "program", "test", "stop");
			System.out.println("control : " + proID + "/program/test/stop");
			started[proID] = false;
		}
		else{
			ccnClient.normalControl(int2StrID(proID), "program", "test", "start");
			System.out.println("control : " + proID + "/program/test/stop");
			started[proID] = true;	
		}

		proID = (int)(Math.random() * NODENUM);
		if(powerOn[proID]){
			ccnClient.normalControl(int2StrID(proID), "power", "state", "false");
			System.out.println("control : " + proID + "/power/state/false");
			started[proID] = false;
		}
		else{
                        ccnClient.normalControl(int2StrID(proID), "power", "state", "true");
                        System.out.println("control : " + proID + "/power/state/true");
                        started[proID] = true;
                }

	}


	public void query(){
		EtData curEtData = ccnClient.normalQuery("all", "all", "all");
		String curNodeID = null;
		for(int i = 0 ; i < ROW; ++i){
			for(int j = 0 ; j < COLUMN; ++j){
				curNodeID = getNodeID(i, j);
				System.out.print(curNodeID + ": ");		
				System.out.print("util = " + curEtData.readData(curNodeID, "cpu", "utilization") + ", ");
				System.out.print("freq = " + curEtData.readData(curNodeID, "cpu", "frequency") + ", ");
				System.out.print("temp = " + curEtData.readData(curNodeID, "cpu", "temperature") + ", ");
				System.out.print("on/off = " + curEtData.readData(curNodeID, "power", "state") + ".\n");

			}
		}


		System.out.println("power : " + curEtData.readData("rack", "outlet", "power"));
		System.out.println("intake : " + curEtData.readData("CRAC", "sensor", "intake"));
		System.out.println("outlet : " + curEtData.readData("CRAC", "sensor", "outlet"));
	}


}
