package ccnToolPackage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class NodePanel extends JPanel{
	public static final int LENTH = 140;
	public static final int WIDTH = 110;
	public static final int BARLEN = 60;
	public static final int BARWID = 30;

	private	String nodeID = null;
	private	JButton detail = null;
	private ProgressBar utilBar = null;
	private ProgressBar freqBar = null;
	private ProgressBar tempBar = null;
	private JRadioButton checkButton = null;
	private JFrame detailFrame = null;
	private DetailPanel detailPanel = null;

	public NodePanel(String nodeID){
		this.nodeID = nodeID;
		initGUI();
	}

	public void initGUI(){
		this.setLayout(null);
		this.setPreferredSize(new Dimension(LENTH, WIDTH));
		//this.setBackground();

		//init detailFrame
		detailFrame = new JFrame();
		detailPanel = new DetailPanel(nodeID);
		detailFrame.add(detailPanel);
		detailFrame.setSize(330, 310);


		//add three progress bars
		utilBar = new ProgressBar(ProgressBar.UTILIZATION);
		freqBar = new ProgressBar(ProgressBar.FREQUENCY);
		tempBar = new ProgressBar(ProgressBar.TEMPERATURE);

		utilBar.setBounds(15 + 0*BARWID + 0*10, 10, BARWID, BARLEN);
		freqBar.setBounds(15 + 1*BARWID + 1*10, 10, BARWID, BARLEN);
		tempBar.setBounds(15 + 2*BARWID + 2*10, 10, BARWID, BARLEN);

		this.add(utilBar);
		this.add(freqBar);
		this.add(tempBar);

		//add radio button

		checkButton = new JRadioButton();

		checkButton.setBounds(15, 2*10 + BARLEN, 20, 20);
		this.add(checkButton);

		//add detail button

		detail = new JButton();
		detail.setMargin(new Insets(0,0,0,0));
		detail.setBounds(55, 2*10 + BARLEN, 70, 20);
		detail.setText(nodeID);
		detail.addActionListener(new DetailListener());
		this.add(detail);

		reset();		

		this.repaint();
	}

	public void reset(){
		setUtil(utilBar.getMax());
		setFreq(freqBar.getMax());
		setTemp(tempBar.getMax());
		setSelected(false);
	}

	public String getNodeID(){
		return nodeID;
	}

	public void setUtil(int util){
		utilBar.setValue(util);
		detailPanel.setUtil(util);
	}

	public void setFreq(int freq){
		freqBar.setValue(freq);
		//detailPanel.setFreq(freq);
	}

	public void setTemp(int temp){
		tempBar.setValue(temp);
		detailPanel.setTemp(temp);
	}

	public void setOnOff(boolean on){
		detailPanel.setOnOff(on);
	}

	public boolean isSelected(){
		return checkButton.isSelected();
	}

	public void setSelected(boolean select){
		checkButton.setSelected(select);
	}

	public static void main(String [] args){
		JFrame jf = new JFrame();
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel jp = new JPanel();
		jp.add(new NodePanel("tarekc12"));
		jp.add(new NodePanel("tarekc13"));
		jf.add(jp);
		jf.setSize(1200, 800);
		jf.setVisible(true);
	}

	class DetailListener implements ActionListener{
		DetailListener(){
		}

		public void actionPerformed(ActionEvent e){
			detailFrame.setVisible(true);

		}
	}

}


class ProgressBar extends JProgressBar{
	public static final int TYPENUM = 3;
	public static final int UTILIZATION = 0;
	public static final int FREQUENCY = 1;
	public static final int TEMPERATURE = 2;
	private static final Color [] typeColor = {Color.RED, Color.BLUE, Color.GREEN};
	private static final int [] minValue = {0, 1197000, 10};
	private static final int [] maxValue = {100, 2395000, 90};

	private int type = 0;
	private Color curColor = null;

	private int value = 0;
	public ProgressBar(int type){
		this.type = type;
		curColor = typeColor[type];
		this.setForeground(curColor);
		this.setOrientation(JProgressBar.VERTICAL);
		this.setMinimum(minValue[type]);
		this.setMaximum(maxValue[type]);
		this.setValue((maxValue[type]));
		this.setPreferredSize(new Dimension(30, 80));
	}

	public int getMax(){
		return maxValue[type];
	}


}

class DetailPanel extends JPanel{
	private int util = 0;
	private int freq = 0;
	private float temp = 0;
	private boolean on = false;
	private boolean selected = false;

	private String nodeID = null;
	private JLabel nameLabel = null;
	private JLabel utilLabel = null;
	private JLabel freqLabel = null;
	private JLabel tempLabel = null;
	private JLabel powerLabel = null;

	private JTextField utilText = null;
	private JTextField freqText = null;
	private JTextField tempText = null;

	private JRadioButton powerStat = null;

	private JButton start = null;
	private JButton stop = null;
	private JButton control = null;

	private ccnClientProxy ccnClient = null;

	public DetailPanel(String nodeID){
		this.nodeID = nodeID;
		ccnClient = new ccnClientProxy();
		initGUI();
	}

	private void initGUI(){
		this.setLayout(null);
		this.setPreferredSize(new Dimension(330, 310));

		//add name label
		nameLabel = new JLabel(this.nodeID);
		nameLabel.setFont(new Font("Dialog", 1, 26));
		nameLabel.setBounds(10, 10, 310, 40);
		nameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(nameLabel);

		//add attribute labels
		utilLabel = new JLabel("Utilization: ");
		freqLabel = new JLabel("Frequency: ");
		tempLabel = new JLabel("Temperature: ");
		powerLabel= new JLabel("Power On: ");

		utilLabel.setFont(new Font("Dialog", 0, 18));
		freqLabel.setFont(new Font("Dialog", 0, 18));
		tempLabel.setFont(new Font("Dialog", 0, 18));
		powerLabel.setFont(new Font("Dialog",0, 18));


		utilLabel.setBounds(10, 60, 150, 30);
		freqLabel.setBounds(10, 100, 150, 30);
		tempLabel.setBounds(10, 140, 150, 30);
		powerLabel.setBounds(10, 180, 150, 30);

		utilLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		freqLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		tempLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		powerLabel.setHorizontalAlignment(SwingConstants.RIGHT);

		this.add(utilLabel);
		this.add(freqLabel);
		this.add(tempLabel);
		this.add(powerLabel);

		//add text fields
		utilText = new JTextField();
		freqText = new JTextField();
		tempText = new JTextField();

		utilText.setBounds(170, 60, 130, 30);
		freqText.setBounds(170, 100, 130, 30);
		tempText.setBounds(170, 140, 130, 30);

		utilText.setEnabled(false);
		tempText.setEnabled(false);

		this.add(utilText);
		this.add(freqText);
		this.add(tempText);

		//add radio button
		powerStat = new JRadioButton();
		powerStat.setEnabled(false);
		powerStat.setBounds(230, 180, 60, 30);
		this.add(powerStat);

		//add button
		start = new JButton("Start");
		stop = new JButton("Stop");
		control = new JButton("Control");

		start.setBounds(35, 220, 80, 30);
		stop.setBounds(125, 220, 80, 30);
		control.setBounds(215, 220, 80, 30);

		control.setMargin(new Insets(0, 0, 0, 0));

		start.addActionListener(new StartListener());
		stop.addActionListener(new StopListener());
		control.addActionListener(new ControlListener());

		this.add(start);
		this.add(stop);
		this.add(control);
	}

	public void setUtil(int util){
		this.util = util;
		utilText.setText("" + util);
	}

	public void setFreq(int freq){
		this.freq = freq;
		freqText.setText("" + freq);	
	}

	public int getFreq(){
		try{
			String curText = freqText.getText();
			return Integer.parseInt(curText);
		}
		catch(Exception ex){
			System.out.println("In NodePanel : getFreq : " + ex.getMessage());
			ex.printStackTrace();
		}
		return 0;
	}

	public void setTemp(float temp){
		this.temp = temp;
		tempText.setText("" + temp);
	}

	public void setOnOff(boolean on){
		this.on = on;
		powerStat.setSelected(on);	
	}

	public boolean getOnOff(){
		return powerStat.isSelected();
	}

	class StartListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			ccnClient.normalControl(nodeID, "program", "test", "start");
		}
	}

	class StopListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			ccnClient.normalControl(nodeID, "program", "test", "stop");		
		}	
	}

	class ControlListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			int curFreq = getFreq();
			ccnClient.normalControl(nodeID, "cpu", "frequency", ""+curFreq);	
			boolean on = getOnOff();
			//ccnClient.normalControl(nodeID, "power", "state", "" + on);	
		}
	}

}

