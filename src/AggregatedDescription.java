package ccnToolPackage;

import ccnToolPackage.*;
import java.sql.Timestamp;
import java.util.*;

/**
 *Energy testbed Data type: the class of return value for queries
 */
public class AggregatedDescription{


	//<For testing begin>
	private static final int DATA_SIZE = 1024;
	private String strData = null;
	private byte [] data = new byte[DATA_SIZE];
	
	public String data2Str(){
		return new String(data);
	}
	//<For testing end>

	/**
	 * This method read data from offset, put it into buffer, and returns how many bytes have been read.
	 * @param buffer	the caller pass this byte pointer to store data.
	 * @param len		buffer length.
	 * @param offset	start offset.
	 * @return		return how many bytes have been read
	 */
	public int read(byte [] buffer, int bufLen, int offset){
		//<For testing begin>
		if (null == data){
			setData("data is null!");
		}
		int length = data.length;
		int remains = length - offset;
		int readCnt = 0;
		if (remains <= 0){
			return 0;
		}
		else if (remains > bufLen){
			readCnt = bufLen;
		}
		else{
			readCnt = remains;
		}

		System.arraycopy(data, offset, buffer, 0, readCnt);
		return readCnt;
		//<For testing End>
	}

	/**
	 *This method write len byte data from buffer to EtData object. The write start from offset.
	 * @param buffer	the caller use this buffer to pass data.
	 * @param len		the number of bytes need bo be written.
	 * @param offset	start offset.
	 */
	public void write(byte [] buffer, int len, int offset){
		//<For testing begin>
		int length = data.length;
		if (length < len + offset){
			byte [] newData = new byte[len + offset];
			System.arraycopy(data, 0, newData, 0, length);
			data = newData;
		}
		System.arraycopy(buffer, 0, data, offset, len);

		//<For testing end>
	}

	//<For testing begin>
	public void setData(String newData){
		strData = newData;
		data = strData.getBytes();
	}
	//<For testing end>

}

