package ccnToolPackage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;

public class ClusterPanel extends JPanel{
	public static final int TOP_MARGIN = 20;
	public static final int ROW = 5;
	public static final int COLUMN = 8;
	public static final int BUTTON_LEN = 80;
	public static final int BUTTON_WID = 20;

	private NodePanel [][] nodePanels = null;
	private DisplayPanel powerPanel = null;
	private DisplayPanel cracPanel = null;
	private JButton allButton = null;
	private JButton noneButton = null;
	private JButton stopButton = null;
	private JButton startButton = null;
	private ccnClientProxy ccnClient = null;
	private EtData curEtData = null;	
	private Timer timer = null;

	public ClusterPanel(){
		ccnClient = new ccnClientProxy();
		initGUI();
	}

	private String int2Str(int nodeIntID){
		if(nodeIntID < 10)
			return "tarekc0" + nodeIntID;
		else
			return "tarekc" + nodeIntID;
	}

	private void initGUI(){
		this.setLayout(null);
		nodePanels = new NodePanel[ROW][COLUMN];
		int i = 0, j = 0;
		int nodeLen = NodePanel.LENTH;
		int nodeWid = NodePanel.WIDTH;
		int nodeID = 0, startPosX = 0, startPosY = 0;

		//add nodes panel
		for(i = 0 ; i < ROW; ++i){
			for(j = 0 ; j < COLUMN; ++j){
				nodeID = i * COLUMN + (j + 1);
				nodePanels[i][j] = new NodePanel(int2Str(nodeID));

				startPosX = 40 + j * nodeLen;
				startPosY = i * nodeWid;
				nodePanels[i][j].setBounds(startPosX, startPosY + TOP_MARGIN, nodeLen, nodeWid);
				this.add(nodePanels[i][j]);
			}
		}

		//add display curve panel
		powerPanel = new DisplayPanel(1, 0, 6000);
		cracPanel = new DisplayPanel(2, 10, 40);

		int displayLen = DisplayPanel.LENTH;
		int displayWid = DisplayPanel.WIDTH;
		powerPanel.setBounds(50, ROW * nodeWid + 2*TOP_MARGIN,  displayLen, displayWid);
		cracPanel.setBounds(50 + displayLen + 10, 2*TOP_MARGIN + ROW * nodeWid, displayLen, displayWid);

		this.add(powerPanel);
		this.add(cracPanel);

		//add buttons
		allButton =  new JButton("All");
		noneButton = new JButton("None");
		stopButton = new JButton("Stop");
		startButton = new JButton("Start");

		allButton.setBounds(50 + 2*displayLen + 2*10, 2*TOP_MARGIN + ROW * nodeWid + 0*BUTTON_WID + 0*12, BUTTON_LEN, BUTTON_WID);
		noneButton.setBounds(50 + 2*displayLen + 2*10, 2*TOP_MARGIN + ROW * nodeWid + 1*BUTTON_WID + 1*12, BUTTON_LEN, BUTTON_WID);
		stopButton.setBounds(50 + 2*displayLen + 2*10, 2*TOP_MARGIN + ROW * nodeWid + 2*BUTTON_WID + 2*12, BUTTON_LEN, BUTTON_WID);
		startButton.setBounds(50 + 2*displayLen + 2*10, 2*TOP_MARGIN + ROW * nodeWid + 3*BUTTON_WID + 3*12, BUTTON_LEN, BUTTON_WID);

		allButton.addActionListener(new AllButtonListener());
		noneButton.addActionListener(new NoneButtonListener());
		stopButton.addActionListener(new StopButtonListener());
		startButton.addActionListener(new StartButtonListener());

		this.add(allButton);
		this.add(noneButton);
		this.add(stopButton);
		this.add(startButton);

	}


	public static void main(String args[]){
		JFrame jf = new JFrame();
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ClusterPanel jp = new ClusterPanel();
		jf.add(jp);
		jf.setSize(1200, 800);
		jf.setVisible(true);	

	}

	class AllButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			for(int i = 0 ; i < ROW; ++i){
				for(int j = 0 ; j < COLUMN; ++j){
					nodePanels[i][j].setSelected(true);
				}
			}
		}	
	}

	class NoneButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			for(int i = 0 ; i < ROW; ++i){
				for(int j = 0 ; j < COLUMN; ++j){
					nodePanels[i][j].setSelected(false);
				}
			}
		}
	}


	private void resetAllNodePanel(){
		for(int i = 0 ; i < ROW; ++i){
			for(int j = 0; j < COLUMN; ++j){
				nodePanels[i][j].reset();
			}
		}
	}

	private int getSelectedNode(){
		int cnt  = 0;
		int nodeNo = -1;
		for(int i = 0 ; i < ROW; ++i){
			for(int j = 0 ; j < COLUMN; ++j){
				if(nodePanels[i][j].isSelected()){
					++cnt;
					nodeNo = i*COLUMN + j + 1;
					if(cnt > 1)
						return -1;
				}
			}
		}

		if(cnt == 0)
			return 0;
		else
			return nodeNo;
	}

	class StopButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(timer != null){
				timer.cancel();
				timer = null;
			}
			powerPanel.stopDisplay();
			cracPanel.stopDisplay();
			resetAllNodePanel();			
		}
	}

	class StartButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			timer = new Timer(true);

			int nodeIntID = getSelectedNode();
			if(nodeIntID < 0){
				timer.schedule(new GetClusterData(), 0, 2000);				
				powerPanel.startDisplay();
				cracPanel.startDisplay();
			}
			else if(nodeIntID > 0){
				int row = (nodeIntID-1) / COLUMN;
				int column = (nodeIntID-1) % COLUMN;
				timer.schedule(new GetNodeData(row, column), 0, 1000);
			}		
		}
	}


	class GetNodeData extends TimerTask{
		int _row = 0;
		int _column = 0;
		public GetNodeData(int row, int column){
			_row = row;
			_column = column;
		}

		public void run(){
			String curNodeID = int2Str(_row*COLUMN + _column + 1);
			curEtData = ccnClient.normalQuery(curNodeID, "all", "all");

			nodePanels[_row][_column].setUtil((int)(100 * curEtData.readData(curNodeID, "cpu", "utilization")));
			nodePanels[_row][_column].setFreq((int)(curEtData.readData(curNodeID, "cpu", "frequency")));
			nodePanels[_row][_column].setTemp((int)(curEtData.readData(curNodeID, "cpu", "temperature")));
			int on = (int)(curEtData.readData(curNodeID, "power", "state"));
			if(on > 0)
				nodePanels[_row][_column].setOnOff(true);
			else
				nodePanels[_row][_column].setOnOff(false);


		}

	}


	class GetClusterData extends TimerTask{
		public void run(){
			curEtData = ccnClient.normalQuery("all", "all", "all");			

			for(int i = 0 ; i < ROW; ++i){
				for(int j = 0 ; j < COLUMN; ++j){
					if(!nodePanels[i][j].isSelected())
						continue;
					String curNodeID = nodePanels[i][j].getNodeID();
					nodePanels[i][j].setUtil((int)(100 * curEtData.readData(curNodeID, "cpu", "utilization")));
					nodePanels[i][j].setFreq((int)(curEtData.readData(curNodeID, "cpu", "frequency")));
					nodePanels[i][j].setTemp((int)(curEtData.readData(curNodeID, "cpu", "temperature")));
					int on = (int)(curEtData.readData(curNodeID, "power", "state"));
					if(on > 0)
						nodePanels[i][j].setOnOff(true);
					else
						nodePanels[i][j].setOnOff(false);

				}
			}


			powerPanel.setNextData((float)(curEtData.readData("rack", "outlet", "power")), 0);
			cracPanel.setNextData((float)(curEtData.readData("CRAC", "sensor", "intake")), 0);
			cracPanel.setNextData((float)(curEtData.readData("CRAC", "sensor", "outlet")), 1);


			repaint();
		}
	}
}
